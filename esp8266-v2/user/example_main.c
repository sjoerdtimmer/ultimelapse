
#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "at.h"

#define sleepms(x) os_delay_us(x*1000);

extern uint8_t at_wifiMode;

void user_init(void)
{

  uart_init(BIT_RATE_115200, BIT_RATE_115200);
  sleepms(3000);
  uart0_sendStr("\r\nsetting wifi  mode to stationary\r\n");
  sleepms(3000);
  at_init();

  //{"+CWMODE", 7, at_testCmdCwmode, at_queryCmdCwmode, at_setupCmdCwmode, NULL},
  uart0_sendStr("\r\nset cwmode\r\n");
  char cwmode[] = "=1\r\n";
  at_setupCmdCwmode(0, cwmode);

  sleepms(3000);
  //{"+CWJAP", 6, NULL, at_queryCmdCwjap, at_setupCmdCwjap, NULL},
  uart0_sendStr("\r\njoin ap\r\n");
  char cwjap[] = "=\"The Promised LAN\",\"1234567890\"\r\n";
  at_setupCmdCwjap(5, cwjap);

  sleepms(3000);
  //{"+CIPMUX", 7, NULL, at_queryCmdCipmux, at_setupCmdCipmux, NULL},
  uart0_sendStr("\r\nset cipmux\r\n");
  char cipmux[] = "=1\r\n";
  at_setupCmdCipmux(0, cipmux);

  sleepms(3000);
  //{"+CIPSTART", 9, at_testCmdCipstart, NULL, at_setupCmdCipstart, NULL},
  uart0_sendStr("\r\ncipstart to server\r\n");
  char cipstart[] = "=4,\"TCP\",\"192.168.178.29\",5000\r\n";
  at_setupCmdCipstart(0, cipstart);

  sleepms(3000);
  //{"+CIPCLOSE", 9, at_testCmdCipclose, NULL, at_setupCmdCipclose, at_exeCmdCipclose},
  uart0_sendStr("\r\ncipclose\r\n");
  char cipclose[] = "=4\r\n";
  at_exeCmdCipclose(0, cipclose);

  sleepms(3000);
  //{"+CWQAP", 6, at_testCmdCwqap, NULL, NULL, at_exeCmdCwqap},
  //at_exeCmdCwqap(0);

  sleepms(3000);
  // 
}