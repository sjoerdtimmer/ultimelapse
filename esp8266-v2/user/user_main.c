#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"
#include "mem.h"
#include "driver/uart.h"
#include "c_types.h"
#include "lwip/pbuf.h"

/* Latest updated architecture:
   There are now three components: all connected to an external AP (see user_config.h for the ssid and password)
   * this esp8266 module which provides a tcp server which sends a message whenever the button is closed
   * an android phone which connects to this module to listen for button messages and uploads pictures to an ftp server 
   * a laptop/pc which runs an ftp server. The also connects to this device to upload the pictures...
   
    FAILED earlier attempt: it turns out that in AP it does not do ip forwarding so the laptop and the phone cannot connect to each other....

*/

/* new general architecture:
    The esp will provide the wifi as well as a tcp server.
    it's the camera's responsibility to connect to that and keep those connections alive or restart them.
    If we were to do it the other way round the esp has to connect to the phone which may not have connected yet resulting in us having to deal with timeouts etc.
    besides starting a tcp server on android is significantly harder than a tcp client while on the esp it's about as difficult.
    A consequnce is that if the tcp connection times out during a trigger event we may miss one frame...
    A problem that we have to be aware of is that sending data on disconnected tcp connections may stall. This is a problem in either of the two described situation.
 */

/* new idea for debouncing the button:
    From now on we just poll in the main loop and call that every 10ms.
    This automatically excludes short presses <10ms.
    I did a small test (on an arduino but nevertheless) and debouncetimes are under 0.5ms and probably way smaller but that's hard to measure. 
*/

// Greatly inspired by 
// https://github.com/cnlohr/ws2812esp8266/blob/master/user/user_main.c
// https://github.com/zarya/esp8266_status_display/blob/master/user/user_buttons.c


/* thinking about tcp connections:
    Atm there are 5 'slots' for tcp connections. If a 6th connection is made 
    it will be refused. However it would be better to have 1 or 2 possible connections 
    for the camera and 3 or 4 for other clients to do http requests.
 */


#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void loop(os_event_t *events);
static volatile os_timer_t my_timer;

uint32 buttonstate = 1; // reflect the default at start (internal pullup so it defaults to HIGH)

LOCAL uint16_t server_timeover = 60*60*12; // yes. 12h timeout. so what? :)

//Max amount of connections
#define MAX_TRIGGER_CONN 8

static struct espconn rfxcomConn;
static esp_tcp rfxcomTcp;
static RfxcomConnData connData[MAX_TRIGGER_CONN];
static volatile os_timer_t keepalive_timer;






//Main code function
static void ICACHE_FLASH_ATTR
loop(os_event_t *events)
{
    // os_printf("new loop\n\r");
    os_delay_us(10000);

    uint32 newstate = GPIO_INPUT_GET(0);
    if(newstate != buttonstate){
        buttonstate = newstate;
        if(buttonstate==0){ // it just went low indicating a press
            rfxcom_trigger();
        }
    }

    system_os_post(user_procTaskPrio, 0, 0 );
}








void ICACHE_FLASH_ATTR
init_wifi_asap(){
    char ssid[32] = SSID;
    struct softap_config wifiConf;

    espconn_tcp_set_max_con(5);
    // Set AP mode
    wifi_set_opmode( SOFTAP_MODE );
    // Configure the AP
    os_memcpy(&wifiConf.ssid, ssid, 32);
    wifiConf.max_connection = 5; // required to reset the client counter for some reason:
    wifi_softap_set_config(&wifiConf);
    // start DHCP
    wifi_softap_dhcps_start();
}



void ICACHE_FLASH_ATTR
init_wifi_asclient(){
    char ssid[32] = SSID;
    char pass[32] = PASSWD;

    // espconn_tcp_set_max_con(5);

    if (wifi_get_opmode() != STATION_MODE) {
        ETS_UART_INTR_DISABLE();
        wifi_set_opmode(  STATIONAP_MODE ); //STATION_MODE
        ETS_UART_INTR_ENABLE();
    }

    struct station_config wifiConf;
    // Configure the AP
    strcpy(wifiConf.ssid, ssid);
    strcpy(wifiConf.password, pass);
    // os_memcpy(&wifiConf.ssid, ssid, 32);
    // os_memcpy(&wifiConf.password, pass, 32);
    wifiConf.bssid_set = 0;    // very important or the scan will not be executed!

    ETS_UART_INTR_DISABLE();
    wifi_station_set_config(&wifiConf);
    wifi_station_connect();
    ETS_UART_INTR_ENABLE();
    wifi_station_dhcpc_start();
}


void ICACHE_FLASH_ATTR
init_wifi_asclientandap(){
    char station_ssid[32] = SSID;
    char station_pass[32] = PASSWD;

    char ap_ssid[32] = AP_SSID;
    // char ap_pass[32] = AP_PASSWD;

    if (wifi_get_opmode() != STATIONAP_MODE) {
        ETS_UART_INTR_DISABLE();
        wifi_set_opmode(  STATIONAP_MODE ); //STATION_MODE
        ETS_UART_INTR_ENABLE();
    }

    // configure the station:
    struct station_config stationConf;
    strcpy(stationConf.ssid, station_ssid);
    strcpy(stationConf.password, station_pass);
    stationConf.bssid_set = 0;    // very important or the scan will not be executed!
    ETS_UART_INTR_DISABLE();
    wifi_station_set_config(&stationConf);
    wifi_station_connect();
    ETS_UART_INTR_ENABLE();
    wifi_station_dhcpc_start();

    // Configure the AP:
    struct softap_config apConf;
    espconn_tcp_set_max_con(5);
    os_memcpy(&apConf.ssid, ap_ssid, 32);
    apConf.max_connection = 5; // required to reset the client counter for some reason:
    ETS_UART_INTR_DISABLE();
    wifi_softap_set_config(&apConf);
    ETS_UART_INTR_ENABLE();
    wifi_softap_dhcps_start();
}




void ICACHE_FLASH_ATTR
rfxcom_keepalive(){
    int i;
    int a;
    a = 0;
    uint8_t *hello = "keepalive\n";
    for(i=0; i<MAX_TRIGGER_CONN; i++) {
        if(connData[i].conn!=NULL)
        {
            espconn_sent(connData[i].conn, hello, 10);
            a=1;
        }
    }
    if (a == 1) os_printf("Sending keepalive\n\r");
}



void ICACHE_FLASH_ATTR
rfxcom_trigger(){
    int i;
    int a;
    a = 0;
    uint8_t *hello = "trigger\n";
    for(i=0; i<MAX_TRIGGER_CONN; i++) {
        if(connData[i].conn!=NULL)
        {
            espconn_sent(connData[i].conn, hello, 8);
            a=1;
        }
    }
    if (a == 1) os_printf("Sending trigger\n\r");
}




static void ICACHE_FLASH_ATTR rfxcomReconCb(void *arg, sint8 err) {
    os_printf("ReconCb\n\r");
}

static void ICACHE_FLASH_ATTR rfxcomDisconCb(void *arg) {
    int i;
    for (i=0; i<MAX_TRIGGER_CONN; i++) {
        if (connData[i].conn!=NULL) {
            if (connData[i].conn->state==ESPCONN_NONE || connData[i].conn->state==ESPCONN_CLOSE) {
                connData[i].conn=NULL;
                os_printf("Disco: %d\n\r",i);
            }
        }
    }
}


static void ICACHE_FLASH_ATTR rfxcomConnectCb(void *arg) {
    //struct espconn *conn=arg;
    // struct espconn *conn = reinterpret_cast<struct espconn*>(arg);

    struct espconn *conn = (struct espconn *) arg;
    int i;
    for (i=0; i<MAX_TRIGGER_CONN; i++) if (connData[i].conn==NULL) break;
    if (i==MAX_TRIGGER_CONN) {
        os_printf("Aiee, conn pool overflow!\n\r");
        espconn_disconnect(conn);
        return;
    }
    connData[i].conn=conn;

    os_printf("Conn %d: \"%d.%d.%d.%d\",%d\n\r",
                   i,IP2STR(conn->proto.tcp->remote_ip),
                   conn->proto.tcp->remote_port);

    // espconn_regist_recvcb(conn, rfxcomRecvCb);
    espconn_regist_reconcb(conn, rfxcomReconCb);
    espconn_regist_disconcb(conn, rfxcomDisconCb);
    espconn_regist_time(conn, 60, 0);
}


void ICACHE_FLASH_ATTR rfxcomInit(int port) {
    int i;

    for (i=0; i<MAX_TRIGGER_CONN; i++)
    {
        connData[i].conn=NULL;
    }

    rfxcomConn.type=ESPCONN_TCP;
    rfxcomConn.state=ESPCONN_NONE;
    rfxcomTcp.local_port=port;
    rfxcomConn.proto.tcp=&rfxcomTcp;

    os_printf("Rfxcom init, conn=%p\n\r", &rfxcomConn);
    espconn_regist_connectcb(&rfxcomConn, rfxcomConnectCb);
    espconn_accept(&rfxcomConn);
    espconn_regist_time(&rfxcomConn, server_timeover, 0);

    os_printf("tcp initialisation done\n\r");

    //Setup keep-alive
    //Disarm timer
    os_timer_disarm(&keepalive_timer);

    //Setup timer
    os_timer_setfn(&keepalive_timer, (os_timer_func_t *)rfxcom_keepalive, NULL);

    //Arm timer for every 5 sec.
    os_timer_arm(&keepalive_timer, 5000, 1);

    os_printf("keepalive system now active...\n\r");
}









#define MAX_HTTP_CONN 3
static RfxcomConnData webConnections[MAX_HTTP_CONN];
static struct espconn httpSocket;
static esp_tcp httpTcp;


static void ICACHE_FLASH_ATTR httpReconCb(void *arg, sint8 err) {}

static void ICACHE_FLASH_ATTR httpDisconCb(void *arg) {
    int i;
    for (i=0; i<MAX_HTTP_CONN; i++) {
        if (webConnections[i].conn!=NULL) {
            if (webConnections[i].conn->state==ESPCONN_NONE || webConnections[i].conn->state==ESPCONN_CLOSE) {
                webConnections[i].conn=NULL;
                os_printf("HTTP disconnect: %d\n\r",i);
            }
        }
    }
}


static void ICACHE_FLASH_ATTR httpRecvCb(void* arg, char *data, unsigned short len){
    struct espconn *conn = (struct espconn *)arg;
    // os_printf("incomming http data: %s", data);
    // char* s = 
    // "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nGPIO is now ";
    // s += (val)?"high":"low";
    // s += "</html>\n";

    // espconn_sent(conn,"",);
}


static void ICACHE_FLASH_ATTR httpConnectCb(void *arg) {
    struct espconn *conn = (struct espconn *) arg;
    int i;
    for (i=0; i<MAX_HTTP_CONN; i++) if (webConnections[i].conn==NULL) break;
    if (i==MAX_HTTP_CONN) {
        os_printf("Aiee, http conn pool overflow!\n\r");
        espconn_disconnect(conn);
        return;
    }
    webConnections[i].conn=conn;

    os_printf("HTTP conn %d: \"%d.%d.%d.%d\",%d\n\r",
                   i,IP2STR(conn->proto.tcp->remote_ip),
                   conn->proto.tcp->remote_port);

    espconn_regist_recvcb(conn, httpRecvCb);
    espconn_regist_reconcb(conn, httpReconCb);
    espconn_regist_disconcb(conn, httpDisconCb);
    espconn_regist_time(conn, 60, 0);
}


void ICACHE_FLASH_ATTR webserverInit() {
    int i;
    for (i=0; i<MAX_HTTP_CONN; i++)
    {
        webConnections[i].conn=NULL;
    }

    httpSocket.type=ESPCONN_TCP;
    httpSocket.state=ESPCONN_NONE;
    httpTcp.local_port=80;
    httpSocket.proto.tcp=&httpTcp;

    os_printf("http socket init, conn=%p\n\r", &httpSocket);
    espconn_regist_connectcb(&httpSocket, httpConnectCb);
    espconn_accept(&httpSocket);
    espconn_regist_time(&httpSocket, server_timeover, 0);
    os_printf("http initialisation done\n\r");

}



//Init function 
void ICACHE_FLASH_ATTR
user_init()
{
    uart_init(BIT_RATE_9600, BIT_RATE_9600);
    init_wifi_asclient();
    rfxcomInit(6000);
    // webserverInit();
    

    // Start os task to keep the watchdog at bay and to poll the button:
    system_os_task(loop, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
    system_os_post(user_procTaskPrio, 0, 0 );

    os_printf("setup done\n\r");
}
