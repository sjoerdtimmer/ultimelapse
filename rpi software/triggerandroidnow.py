#!/bin/python3
import socket
import sys

if len(sys.argv) is not 2:
	print("Usage: %s address"%sys.argv[0])
	sys.exit(-1)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.sendto(bytes("do it!", "utf-8"), (sys.argv[1], 7654))
