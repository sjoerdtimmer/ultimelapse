#!/bin/python3

# wiring taken from: http://www.raspberrypi.org/learning/python-picamera-setup/
# raspifastcamd taken from: http://www.raspberrypi.org/forums/viewtopic.php?f=43&t=50075

import time
import RPi.GPIO as GPIO
import subprocess
import os.path
import signal

# some locations:
scriptlocation = os.path.dirname(os.path.realpath(__file__))
imagelocation = os.path.abspath(os.path.join(scriptlocation, '../images/'))

# init GPIO:
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, GPIO.PUD_UP)

# init camera capture:
print("starting fastcamd...",end="")
fastcamd = subprocess.Popen("./raspifastcamd -w 1280 -h 720 -e png -o %s/%%030d.png"%imagelocation,shell=True,cwd=scriptlocation);


# setup handler for Ctrl-C:
def onCtrlC(signal,frame):
	print("gracefully stopping the camera...")
	fastcamd.terminate()	
	sys.exit(0)
signal.signal(signal.SIGINT,onCtrlC)

# main loop:
print("done.\ngoing into main loop.\npress Ctrl+C to quit...")
while True:
    print("waiting for trigger...",end="")
    GPIO.wait_for_edge(17, GPIO.FALLING)
    print("taking a picture...",end="")
    fastcamd.send_signal(signal.SIGUSR1)
    time.sleep(0.2) # debounce time
    print("done")


