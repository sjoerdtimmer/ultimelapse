#!/bin/python3

TARGETIP = '192.168.199.2'

import time
import RPi.GPIO as GPIO
import os.path
import socket
import signal

# init GPIO:
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, GPIO.PUD_UP)

# init camera capture:
print("starting network...",end="")

# init udp socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP

# setup handler for Ctrl-C:
def onCtrlC(signal,frame):
	print("gracefully stopping the network...")
	sock.close()
	sys.exit(0)
signal.signal(signal.SIGINT,onCtrlC)

# main loop:
print("done.\ngoing into main loop.\npress Ctrl+C to quit...")
while True:
    print("waiting for trigger...",end="")
    GPIO.wait_for_edge(17, GPIO.FALLING)
    print("taking a picture...",end="")
    time.sleep(0.2) # debounce time and wait a bit until the printhead has stopped moving
    
    sock.sendto(bytes("do it!", "utf-8"), (TARGETIP, 7654))

    print("done")

