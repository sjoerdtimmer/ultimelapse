#Name: Photo Trigger 2
#Info: Trigger a switch after every layer
#Depend: GCode
#Type: postprocess
#Param: triggerX1(float:0) Trigger X (mm)
#Param: triggerY1(float:200) Trigger Y (mm)
#Param: retractAmount(float:5) Retraction amount (mm)
#Param: liftDist(float:0.5) Lift distance (mm)
#Param: waittime(float:1000) Delay (ms)

__copyright__ = "Copyright (C) 2015 Sjoerd T. Timmer - Released under terms of the AGPLv3 License"
import re

def getValue(line, key, default = None):
	if not key in line or (';' in line and line.find(key) > line.find(';')):
		return default
	subPart = line[line.find(key) + 1:]
	m = re.search('^[0-9]+\.?[0-9]*', subPart)
	if m is None:
		return default
	try:
		return float(m.group(0))
	except:
		return default

with open(filename, "r") as f:
	lines = f.readlines()


z = 0.
x = 0.
y = 0.
with open(filename, "w") as f:
	for line in lines:
		if line.startswith(';LAYER:'):
			print('woohoo')
			# do the warp move:
			f.write(";TYPE:CUSTOM\n")                              # document what happens
			f.write("M83\n")                                       # set extruder to relative
			f.write("G1 E-%f F6000\n" % (retractAmount))           # retract
			f.write("G1 Z%f F300\n" % (z+float(liftDist)))         # move up a bit
			f.write("G1 X%f Y%f F9000\n" % (triggerX1, triggerY1)) # hit the trigger
			f.write("G4 P%f\n" % waittime)                         # wait
			f.write("G1 X%f Y%f F9000\n" % (x, y))                 # move back
			f.write("G1 E%f F6000\n" % (retractAmount))            # undo retraction
			f.write("M82\n")                                       # back to absolute mode
			# I rely on a Z move to move down. All layers start with a z change, but 
			# some layers move to a new location first because the place where they ended is not actually in the next layer
			# therefore I also don't want to move down just yet...
			

		# keep track of where we are
		if getValue(line, 'G', None) == 1 or getValue(line, 'G', None) == 0:
			z = getValue(line, 'Z', z)
			x = getValue(line, 'X', x)
			y = getValue(line, 'Y', y)

		# output the original:
		f.write(line)



