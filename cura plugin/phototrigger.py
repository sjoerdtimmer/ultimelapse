#Name: Photo Trigger
#Info: Trigger a switch after every layer
#Depend: GCode
#Type: postprocess
#Param: triggerX1(float:100) Trigger X (mm)
#Param: triggerY1(float:100) Trigger Y (mm)
#Param: retractAmount(float:5) Retraction amount (mm)
#Param: waittime(float:200) Delay (ms)

__copyright__ = "Copyright (C) 2014 Sjoerd T. Timmer - Released under terms of the AGPLv3 License"
import re

def getValue(line, key, default = None):
	if not key in line or (';' in line and line.find(key) > line.find(';')):
		return default
	subPart = line[line.find(key) + 1:]
	m = re.search('^[0-9]+\.?[0-9]*', subPart)
	if m is None:
		return default
	try:
		return float(m.group(0))
	except:
		return default

with open(filename, "r") as f:
	lines = f.readlines()

z = 0.
x = 0.
y = 0.
pauseState = 0
currentSectionType = 'STARTOFFILE'
with open(filename, "w") as f:
	for line in lines:
		if line.startswith(';'):
			if line.startswith(';TYPE:'):
				currentSectionType = line[6:].strip()
			f.write(line)
			continue
		if getValue(line, 'G', None) == 1 or getValue(line, 'G', None) == 0:
			newZ = getValue(line, 'Z', z)
			print(newZ)
			x = getValue(line, 'X', x)
			y = getValue(line, 'Y', y)
			if newZ != z and currentSectionType != 'CUSTOM':
				z = newZ
				f.write(";TYPE:CUSTOM\n")
				#Retract a little
				f.write("M83\n")
				f.write("G1 E-%f F6000\n" % (retractAmount))
				# to prevent dragging the head through the top layer we move to the new z first
				f.write("G1 Z%f F300\n" % z)
				# the we move to the pretigger location
				# push the trigger
				f.write("G1 X%f Y%f F9000\n" % (triggerX1, triggerY1))
				#f.write("G1 X%f Y%f F9000\n" % (triggerX2, triggerY2))
				f.write("G4 P%f\n" % waittime)
				# move back to the object
				f.write("G1 X%f Y%f F9000\n" % (x, y))
				# and undo retraction:
				f.write("G1 E%f F6000\n" % (retractAmount))
				f.write("G1 F9000\n")
				# and continue printing, probably the first line will be to move z which we already did so that has no effect. shouldn't hurt either
				f.write("M82\n")
		f.write(line)


# http://www.thingiverse.com/thing:123218/#files
# http://www.thingiverse.com/thing:244320/#files