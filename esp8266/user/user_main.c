#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"
// #include "espconn.h"
#include "mem.h"
#include "driver/uart.h"
#include "c_types.h"

#include "lwip/pbuf.h"
// #include "lwip/ip_addr.h"



// Greatly inspired by 
// https://github.com/cnlohr/ws2812esp8266/blob/master/user/user_main.c
// https://github.com/zarya/esp8266_status_display/blob/master/user/user_buttons.c

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void loop(os_event_t *events);

static volatile os_timer_t my_timer;
static volatile os_timer_t debounce_timer;
// static volatile os_timer_t udp_timer;





//Main code function
static void ICACHE_FLASH_ATTR
loop(os_event_t *events)
{
    // os_printf("new loop\n\r");
    os_delay_us(10000);

    system_os_post(user_procTaskPrio, 0, 0 );
}



/*void ICACHE_FLASH_ATTR
on_upd_receive(void *data){
    os_printf("got some data...\n\r");
}
*/





void ICACHE_FLASH_ATTR
init_wifi(){
    char ssid[32] = SSID;
    struct softap_config wifiConf;

    espconn_tcp_set_max_con(5);
    // Set AP mode
    wifi_set_opmode( SOFTAP_MODE );
    // Configure the AP
    os_memcpy(&wifiConf.ssid, ssid, 32);
    wifiConf.max_connection = 5; // required to reset the client counter for some reason:
    wifi_softap_set_config(&wifiConf);
    // start DHCP
    wifi_softap_dhcps_start();
}


struct pbuf* p;
struct ip_addr ipgroup;
struct udp_pcb *g_udppcb;
char msg[] = "A";



init_udp(){    
    IP4_ADDR(&ipgroup, 192, 168, 4, 255 ); //MultiCasting Ipaddress.    
}

void ICACHE_FLASH_ATTR
send_lwip_udp(){
    // os_printf("sending udp packet...\n\r");

    p = pbuf_alloc(PBUF_TRANSPORT,sizeof(msg),PBUF_RAM);
    memcpy (p->payload, msg, sizeof(msg));
    
    g_udppcb =( struct udp_pcb*) udp_new();

    udp_sendto(g_udppcb,p,&ipgroup,MYPORT); //send a multicast packet
    // os_printf("packet sent!!!\n\r");
    pbuf_free(p);
}



void ICACHE_FLASH_ATTR
debounce_timer_callback(void *arg) {
    if(GPIO_INPUT_GET(0)){
        os_printf("the pin is now low\n\r");
    }else{
        os_printf("the pin is now high... trigger\n\r");
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
        os_delay_us(50000);
        send_lwip_udp();
    }
    gpio_pin_intr_state_set(GPIO_ID_PIN(0), GPIO_PIN_INTR_ANYEGDE);
    ETS_GPIO_INTR_ENABLE();
}

void ICACHE_FLASH_ATTR
handle_pinchange(){
    ETS_GPIO_INTR_DISABLE();

    os_printf("GPIO Interrupt received!!!\n\r");
    // disable consecutive triggers to ignore debounce
       //clear interrupt status:
    uint32 gpio_status = GPIO_REG_READ(GPIO_STATUS_ADDRESS);
    gpio_pin_intr_state_set(GPIO_ID_PIN(0), GPIO_PIN_INTR_DISABLE);
    GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpio_status & BIT(0));

    // start the debounce timer: after 50ms interrupts will be reenabled
    os_timer_disarm(&debounce_timer);
    os_timer_setfn(&debounce_timer, (os_timer_func_t *)debounce_timer_callback, 0);
    os_timer_arm(&debounce_timer, DOUBLEPURPOSEDELAY, 0);
}


void ICACHE_FLASH_ATTR
init_button(){
    // setup pin handling:
    gpio_init();

    ETS_GPIO_INTR_ATTACH(handle_pinchange,0);
    ETS_GPIO_INTR_DISABLE();
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
    gpio_output_set(0, 0, 0, GPIO_ID_PIN(0));
    gpio_register_set(GPIO_PIN_ADDR(0), GPIO_PIN_INT_TYPE_SET(GPIO_PIN_INTR_DISABLE)
              | GPIO_PIN_PAD_DRIVER_SET(GPIO_PAD_DRIVER_DISABLE)
              | GPIO_PIN_SOURCE_SET(GPIO_AS_PIN_SOURCE));

    GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(0));

    gpio_pin_intr_state_set(GPIO_ID_PIN(0), GPIO_PIN_INTR_ANYEGDE);
    // gpio_intr_handler_register(handle_pinchange,0); 

    ETS_GPIO_INTR_ENABLE();
}



//Init function 
void ICACHE_FLASH_ATTR
user_init()
{
    uart_init(BIT_RATE_9600, BIT_RATE_9600);

    init_wifi();
    os_printf("wifi should be up now...\n\r");

    init_udp();

    // Start os task
    system_os_task(loop, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
    system_os_post(user_procTaskPrio, 0, 0 );

    init_button();

    os_printf("setup done\n\r");
}
