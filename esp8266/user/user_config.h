#define SSID "NSA Hotspot"
#define MYPORT 7654
#define LOCAL_PORT 12345

/* the following constant has a double purpose. 
It is used for the debounce time (after every polarity change no further changes are detected withing this number of ms).
Secondly, the trigger packets are not immediately sent out on trigger, but only after the debounce time is over.
This enables the printhead to assume a stable position. The recommended value is half of the period for which the printhead is parked in the trigger corner.
*/
#define DOUBLEPURPOSEDELAY 200

#define IP_ADDR_ANY          ((u32_t)0x00000000UL)
// #define MAKE_IP4_ADDR(ipaddr, a,b,c,d)     (ipaddr) = ((uint32)((d) & 0xff) << 24) | ((uint32)((c) & 0xff) << 16) | ((uint32)((b) & 0xff) << 8)  | (uint32)((a) & 0xff)