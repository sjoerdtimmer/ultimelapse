# triggered timelapse movies on an Ultimaker #

Time-lapse movies of 3d printing are generally poised by the print head jabbering through the images. To make 'clean' time-lapse movies I made a cura plugin that moves the y axis to the far end of the bed after every layer. A micro switch located there signals a raspberry pi to take a picture. There are two different methods for taking the picture: either using a camera module or by hooking up an android phone. I first developed the raspberry-pi camera module version but noticed that the image quality in rather poor in the lighting conditions as they are in our space. Also, there is very little control over the automatic whitebalancing in the camera which makes the timelapse look quite unstable.

First results:
https://www.youtube.com/watch?v=pqvD_04vEsA


This repo contains the cura plugin, scripts for the raspberry pi and some 3d-printed parts for easy mounting of the camera module and the micro switch.
![IMG_20140701_235530.jpg](https://bytebucket.org/sjoerdtimmer/ultimelapse/raw/master/media/IMG_20140701_235530.jpg)
![IMG_20140701_235546.jpg](https://bytebucket.org/sjoerdtimmer/ultimelapse/raw/master/media/IMG_20140701_235546.jpg)
![IMG_20140701_235557.jpg](https://bytebucket.org/sjoerdtimmer/ultimelapse/raw/master/media/IMG_20140701_235557.jpg)
![IMG_20140701_235634.jpg](https://bytebucket.org/sjoerdtimmer/ultimelapse/raw/master/media/IMG_20140701_235634.jpg)
![IMG_20140701_235652.jpg](https://bytebucket.org/sjoerdtimmer/ultimelapse/raw/master/media/IMG_20140701_235652.jpg)



### setup ###
* print the parts(cameraclip twice and camerahook twice once mirrored)
* connect the switch between pin 17 and gnd
* mount the camera (you will need to find something to keep the pi in place)
* put the cura plugin in /usr/share/cura plugins (or equivalent)
* start cura and enable the plugin, use the "move axis" feature of the ultimaker to find the correct y value.
* run the python script (as root) 
* check the alignment, hit the switch a few times and check that the view is good. Don't use raspivid because it crops the images to fit the currently attached monitor... 
* start the print




### Tested with/developed for ###

* python 3 with gpio package
* a configured camera module
* tested with cura 14.03
* 3d mounting brackets designed for ultimaker original