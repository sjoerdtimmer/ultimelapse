package com.themba.triggerlapse;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Environment;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import android.net.wifi.WifiManager;
import android.net.DhcpInfo;
import android.widget.TextView;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;


public class Main extends Activity {
    private static final String TAG = "CamTestActivity";
    private static final int SERVER_PORT = 6000;
    private static final String SERVER_IP = "192.168.199.126";

    Preview preview;
    Button buttonTrigger;
    Button buttonFocus;
    SeekBar exposureBar,focusBar,whiteBar;
    Camera camera;
    String fileName;
    Activity act;
    Context ctx;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        act = this;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);


        SurfaceView surface = (SurfaceView) findViewById(R.id.videoSurface);
        preview = new Preview(this, surface);
        LinearLayout layout = ((LinearLayout) findViewById(R.id.layout));
        LinearLayout vertical = ((LinearLayout) findViewById(R.id.vertical));
        vertical.addView(preview);
        preview.setKeepScreenOn(true);

        try {
            TextView textView2 = (TextView) findViewById(R.id.textView2);
            textView2.setText(getIPAddress().toString());
        } catch (Exception e) {
            System.out.println("error obtaining ip address");
            e.printStackTrace();
        }

        buttonTrigger = (Button) findViewById(R.id.triggerButton);
        buttonFocus = (Button) findViewById(R.id.focusButton);


        buttonTrigger.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(shutterCallback, rawCallback, jpegCallback);
            }
        });

        buttonFocus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Camera.Parameters params = camera.getParameters();
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
                camera.setParameters(params);
                if (camera.getParameters().getMaxNumFocusAreas() > 0) {
                    camera.getParameters().setFocusAreas(new LinkedList<Camera.Area>() {
                        {
                            Rect r = new Rect();
                            r.set(-100, -100, 100, 100);
                            this.add(new Camera.Area(r, 500));
                        }
                    });
                }
                camera.autoFocus(new AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean arg0, Camera arg1) {
                        System.err.println("focussing ");
                    }
                });
            }
        });


        exposureBar = (SeekBar) findViewById(R.id.exposureBar);

        whiteBar = (SeekBar) findViewById(R.id.whiteBar);

        exposureBar.setMax(4);
        exposureBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (camera != null) {
                    Camera.Parameters params = camera.getParameters();
                    params.setExposureCompensation(progress - 2);
                    camera.setParameters(params);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        // lets do some networking:
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    DatagramSocket socket = new DatagramSocket(7654);
//                    byte[] buf = new byte[1024];
//                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
//                    while(true) {
//                        Log.v("triggerlapse","Hello World");
//                        try {
//                            socket.receive(packet);
//                            System.out.println("got some data: " + packet.getData());
//                            camera.takePicture(shutterCallback, rawCallback, jpegCallback);
//                        }catch(IOException e){
//                            System.err.println("IO Exception");
//                            e.printStackTrace();
//                        }catch(Exception e){
//                            System.err.println("unknown exception occured");
//                            e.printStackTrace();
//                        }
//                    }
//                }catch(SocketException e){
//                    System.err.println("could not open network socket!");
//                    e.printStackTrace();
//                }
//            }
//        }).start();
        // lets do some networking:
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        Socket socket = new Socket(InetAddress.getByName(SERVER_IP), SERVER_PORT);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        System.err.println("socket now open...");
                        while (true) {
                            String line = reader.readLine();
                            System.err.println("got \"" + line + "\"");

                            if ((camera != null) && (line.equals("trigger"))) { // line is null if no data available, newline characters are not included
                                System.err.println("taking a picture...");
                                camera.takePicture(shutterCallback, rawCallback, jpegCallback);
                            }
                        }
                    } catch (SocketException e) {
                        System.err.println("could not open network socket!");
                        //                    e.printStackTrace();
                    } catch (UnknownHostException e) {
                        System.err.print("unknown host!");
                        //                    e.printStackTrace();
                    } catch (IOException e) {
                        System.err.println("IO Exception");
                        //                    e.printStackTrace();
                    } catch (Exception e) {
                        System.err.println("unknown exception occured");
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        System.err.println("exception occured during sleep");
                    }
                    System.err.println("retrying now...");
                }
            }
        }).start();
    }


    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();

        camera.startPreview();
        preview.setCamera(camera);
    }

    @Override
    protected void onPause() {
        if(camera != null) {
            camera.stopPreview();
            preview.setCamera(null);
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    private void resetCam() {
        camera.startPreview();
        preview.setCamera(camera);
    }

    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
            // Log.d(TAG, "onShutter'd");
        }
    };

    PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            // Log.d(TAG, "onPictureTaken - raw");
        }
    };

    PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        FileOutputStream outStream = null;
        try {
            /* Checks if external storage is available for read and write */
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                System.out.println("external storage available");
            }else {
                System.out.println("external storage not available");
            }

            // Write to SD Card
            File extStore = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            System.out.println("external storage location: "+extStore);
//            fileName = String.format("%s/TriggerLapse/%d.jpg", extStore, System.currentTimeMillis());
//            fileName = String.format("/mnt/sdcard/DCIM/TriggerLapse/%d.jpg", System.currentTimeMillis());
//            System.out.format("Public picture storage=%s\n",Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
//            System.out.format("Public FCIM storage=%s\n",Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM));
//            for(File s: getExternalFilesDirs(null)){
//                System.out.format("external storage: %s\n",s);
//            }
            // for strange reasons we need to use the second storage location to get things on the SD card and writing to DCIM/TriggerLaper is not allowed:
            fileName = String.format("/storage/sdcard1/Android/data/com.themba.triggerlapse/%d.jpg", System.currentTimeMillis());
            System.out.format("storing file in %s\n",fileName);
            outStream = new FileOutputStream(fileName);
            outStream.write(data);
            outStream.close();

            MyFtpUploader ftp = new MyFtpUploader();
            ftp.setData(data);
            new Thread(ftp).start();

            resetCam();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
    };

    InetAddress getIPAddress() throws IOException {
        WifiManager wifi = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();

        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((dhcp.ipAddress >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

    InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }
}