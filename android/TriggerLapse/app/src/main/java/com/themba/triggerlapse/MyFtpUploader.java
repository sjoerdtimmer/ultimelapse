package com.themba.triggerlapse;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by sjoerd on 4/6/15.
 */
public class MyFtpUploader implements Runnable {

    byte[] data;
    public void setData(byte[] data){
        this.data = data;
    }

    public void run(){
        FTPClient ftpClient = new FTPClient();
        try {

//            ftpClient.connect("192.168.2.4", 21);  // at home
            ftpClient.connect("192.168.199.104", 21); // at randomdata
            ftpClient.login("john", "doe");
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            String secondRemoteFile = String.format("%d.jpg", System.currentTimeMillis());
            //                inputStream = new FileInputStream(secondLocalFile);

//            System.out.println("Start uploading second file");
            OutputStream outputStream = ftpClient.storeFileStream(secondRemoteFile);
            System.out.println(outputStream);

            int l = this.data.length;

            outputStream.write(this.data);
//            for(int i=0;i<this.data.length;i++){
//                outputStream.write(data[i]);
//                System.err.print(".");
//            }




//            System.err.println("sending done...");

            outputStream.close();
//            System.err.println("OK");

            boolean completed = ftpClient.completePendingCommand();

            if (completed) {
                System.err.println("transfer complete!");
//                System.out.println("The second file is uploaded successfully.");
            }

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }



}
